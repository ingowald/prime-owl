// ======================================================================== //
// Copyright 2019 Ingo Wald                                                 //
//                                                                          //
// Licensed under the Apache License, Version 2.0 (the "License");          //
// you may not use this file except in compliance with the License.         //
// You may obtain a copy of the License at                                  //
//                                                                          //
//     http://www.apache.org/licenses/LICENSE-2.0                           //
//                                                                          //
// Unless required by applicable law or agreed to in writing, software      //
// distributed under the License is distributed on an "AS IS" BASIS,        //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. //
// See the License for the specific language governing permissions and      //
// limitations under the License.                                           //
// ======================================================================== //

#include "prql/prql.h"
#include "prql/common.h"
#include "prql/Device.h"

namespace prql {
  
  PRQ_API void prqInit(int, const char **)
  {
    std::cout << "#prql: initializing prql library" << std::endl;
  }

  PRQ_API void prqFinalize()
  {
    std::cout << "#prql: shutting down..." << std::endl;
  }
  
  PRQ_API PRQDevice prqDeviceCreate(PRQTarget target, int deviceID)
  {
    std::cout << "#prql: deviceCreate: ignoring target and deviceID..." << std::endl;
    if (getenv("PRQL_EMBREE"))
      return (PRQDevice)prql::createEmbreeDevice();
    else
      return (PRQDevice)prql::createOptixDevice();
  }

  PRQ_API void      prqDeviceDestroy(PRQDevice device)
  {
    std::cout << "#prql: destoying device..." << std::endl;
    assert(device);
    delete (Device *)device;
  }

  PRQ_API void      prqSceneDestroy(PRQScene scene)
  {
    std::cout << "#prql: destoying scene..." << std::endl;
    assert(scene);
    delete (Scene *)scene;
  }
  

  PRQ_API PRQScene prqSceneCreate(PRQDevice _device,
                                  int numGeometries,
                                  int numObjects,
                                  int numInstances)
  {
    assert(_device);
    Device *device = (Device *)_device;
    return device->createScene(numGeometries,numObjects,numInstances);
  }


  PRQ_API void prqSceneFinalize(PRQScene _scene)
  {
    assert(_scene);
    Scene *scene = (Scene *)_scene;
    scene->finalize();
  }

  PRQ_API void prqGeometryTriangles(PRQScene _scene,
                                    int geomID,
                                    size_t triangleCount,
                                    const void *triangles,
                                    size_t sizeOfTriangle=9*sizeof(float),
                                    size_t sizeOfVertex=3*sizeof(float),
                                    size_t offsetFirstVertex=0)
  {
    assert(_scene);
    Scene *scene = (Scene *)_scene;
    assert(scene);
    
    Geometry *geometry =
      scene->createTriangles(geomID,
                             triangleCount,
                             triangles,
                             sizeOfTriangle,
                             sizeOfVertex,
                             offsetFirstVertex);
    assert(geometry);

    assert(geomID >= 0);
    assert(scene->geometries[geomID] == nullptr);
    
    assert(geometry);
    scene->geometries[geomID] = geometry;
  }

  /*! waits for an event to complete, and returns number of rays that
    did have a valid intersection */
  PRQ_API size_t prqWaitFor(PRQEvent _event)
  {
    assert(_event);
    Event *event = (Event *)_event;
    size_t result = event->waitForCompletion();
    
    delete event;
    return result;
  }
  
  /*! perform a closest hit operation on given rays; after completion of
    this operation the 'isec' array should contain, for each passed
    ray, the closest inteserction in the ray.{tmin,tmax} interval */
  PRQ_API PRQEvent prqFindClosestHit(PRQScene _scene,
                                     PRQRay  *rays,
                                     PRQIsec *isecs,
                                     size_t   count)
  {
    assert(_scene);
    Scene *scene = (Scene *)_scene;

    if (count == 0) return 0;
    
    assert(rays);
    assert(isecs);
    
    return (PRQEvent)scene->findClosestHit((Ray *)rays,(Isec *)isecs,count);
  }
  
}
