// ======================================================================== //
// Copyright 2019 Ingo Wald                                                 //
//                                                                          //
// Licensed under the Apache License, Version 2.0 (the "License");          //
// you may not use this file except in compliance with the License.         //
// You may obtain a copy of the License at                                  //
//                                                                          //
//     http://www.apache.org/licenses/LICENSE-2.0                           //
//                                                                          //
// Unless required by applicable law or agreed to in writing, software      //
// distributed under the License is distributed on an "AS IS" BASIS,        //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. //
// See the License for the specific language governing permissions and      //
// limitations under the License.                                           //
// ======================================================================== //

#include "prql/Device.h"

namespace prql {
  namespace op {

    struct Scene : public prql::Scene
    {
      Scene(int numGeometries,
            int numObjects,
            int numInstances);
      void finalize() override;
    
      Geometry *createTriangles(int geomID,
                                size_t triangleCount,
                                const void *triangles,
                                size_t sizeOfTriangle,
                                size_t sizeOfVertex,
                                size_t offsetFirstVertex) override;

      Event *findClosestHit(Ray *rays,
                            Isec *isecs,
                            size_t count) override;
    };
      
    struct Device : public prql::Device
    {
      PRQScene createScene(int numGeometries,
                           int numObjects,
                           int numInstances) override
      {
        return (PRQScene)new Scene(numGeometries,
                                   numObjects,
                                   numInstances);
      }
    };

    Scene::Scene(int numGeometries,
                 int numObjects,
                 int numInstances)
      : prql::Scene(numGeometries,numObjects,numInstances)
    {}

    void Scene::finalize() 
    {
      PING; GDT_NOTIMPLEMENTED;
    }
    
    Geometry *Scene::createTriangles(int geomID,
                                     size_t triangleCount,
                                     const void *triangles,
                                     size_t sizeOfTriangle,
                                     size_t sizeOfVertex,
                                     size_t offsetFirstVertex) 
    {
      PING; GDT_NOTIMPLEMENTED;
    }

    Event *Scene::findClosestHit(Ray *rays,
                                 Isec *isecs,
                                 size_t count) 
    {
      PING; GDT_NOTIMPLEMENTED;
    }
    
  };
  
  Device *createEmbreeDevice()
  {
    PING; GDT_NOTIMPLEMENTED;
  }
  
  Device *createOptixDevice()
  {
    return new op::Device;
  }

} // ::prql
