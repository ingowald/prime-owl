// ======================================================================== //
// Copyright 2019 Ingo Wald                                                 //
//                                                                          //
// Licensed under the Apache License, Version 2.0 (the "License");          //
// you may not use this file except in compliance with the License.         //
// You may obtain a copy of the License at                                  //
//                                                                          //
//     http://www.apache.org/licenses/LICENSE-2.0                           //
//                                                                          //
// Unless required by applicable law or agreed to in writing, software      //
// distributed under the License is distributed on an "AS IS" BASIS,        //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. //
// See the License for the specific language governing permissions and      //
// limitations under the License.                                           //
// ======================================================================== //

#pragma once

#include "prql/prql.h"
#include "prql/common.h"

namespace prql {

  struct Ray {
    vec3f org;
    float tmin;
    vec3f dir;
    float tmax;
  };

  struct Isec {
    int instID;
    int geomID;
    int primID;
    float t;
    float u;
    float v;
    int reserved[2];
  };
  
  struct Device {
    virtual PRQScene createScene(int numGeometries,
                                 int numObjects,
                                 int numInstances) = 0;
    virtual ~Device() {}
  };

  struct Geometry {
    virtual ~Geometry() {}
  };

  struct Event
  {
    virtual size_t waitForCompletion() = 0;
  };

  struct Object {
    int foo;
  };
  
  struct Instance {
    int foo;
  };
  
  struct Scene
  {
    Scene(int numGeometries,
          int numObjects,
          int numInstances)
      : geometries(numGeometries),
        objects(numObjects),
        instances(numInstances)
    {}

    virtual void finalize() = 0;
    
    virtual ~Scene() {}

    virtual Geometry *createTriangles(int geomID,
                                      size_t triangleCount,
                                      const void *triangles,
                                      size_t sizeOfTriangle,
                                      size_t sizeOfVertex,
                                      size_t offsetFirstVertex) = 0;

    virtual Event *findClosestHit(Ray *rays,
                                  Isec *isecs,
                                  size_t count) = 0;

    std::vector<Geometry *> geometries;
    std::vector<Object>     objects;
    std::vector<Instance>   instances;
  };

  struct TriangleMesh : public Geometry {
  };
  

  Device *createEmbreeDevice();
  Device *createOptixDevice();
  
} // ::prql
