// ======================================================================== //
// Copyright 2019 Ingo Wald                                                 //
//                                                                          //
// Licensed under the Apache License, Version 2.0 (the "License");          //
// you may not use this file except in compliance with the License.         //
// You may obtain a copy of the License at                                  //
//                                                                          //
//     http://www.apache.org/licenses/LICENSE-2.0                           //
//                                                                          //
// Unless required by applicable law or agreed to in writing, software      //
// distributed under the License is distributed on an "AS IS" BASIS,        //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. //
// See the License for the specific language governing permissions and      //
// limitations under the License.                                           //
// ======================================================================== //

#pragma once

#include <stdint.h>
#include <stdlib.h>

typedef uint8_t PRQMask;

#define PRQ_API extern "C"

typedef struct _prq3f {
  float x;
  float y;
  float z;
} prq3f;
  
typedef enum
  {
   /*! everything lives on, and 100% remains, on the host at all
     times */
   PRQ_HOST_ONLY,
   
   /*! functions get _called_ on the host, but all data - INCLUDING
     the rays[] and isecs[] - live on the device (typically, this
     means that these arrays are created with cudaMalloc, and written
     to/read from cuda kernels)*/
   
   PRQ_DEVICE_ONLY,
   /*! application lives on the host, and consequently, rays[] and
     isecs[] arrays are both in host memory - but scene gets created
     on the devicea, and tracing happens on the device */
   PRQ_DEVICE_OFFLOAD
  }
  PRQTarget;

typedef struct _PRQDevice *PRQDevice;
typedef struct _PRQScene  *PRQScene;

PRQ_API void prqInit(int, const char **);
PRQ_API void prqFinalize();

// ------------------------------------------------------------------
//
// ------------------------------------------------------------------

PRQ_API PRQDevice prqDeviceCreate(PRQTarget target,
                                  int deviceID);
PRQ_API void      prqDeviceDestroy(PRQDevice device);

// ------------------------------------------------------------------
//
// ------------------------------------------------------------------

PRQ_API PRQScene prqSceneCreate(PRQDevice device,
                                int numGeometries,
                                int numObjects=1,
                                int numInstances=0);

PRQ_API void prqSceneFinalize(PRQScene scene);

PRQ_API void prqSceneDestroy(PRQScene scene);

// ------------------------------------------------------------------
//
// ------------------------------------------------------------------

PRQ_API void prqInstanceCreate(PRQScene scene,
                               int instID,
                               const float *xfm,
                               int objID);
PRQ_API void prqInstanceSetMask(PRQScene scene,
                                int instID,
                                PRQMask mask);

// ------------------------------------------------------------------
//
// ------------------------------------------------------------------

PRQ_API void prqObjectCreate(PRQScene scene,
                             int objID,
                             int beginGeomID,
                             int endGeomID);

PRQ_API void prqObjectDestroy(PRQScene scene,
                              int objID);
                     
// ------------------------------------------------------------------
// Geometries
// ------------------------------------------------------------------

/*! create a triangle geometry specified through an index array and a
  vertex array */
PRQ_API void prqGeometryTriangleMesh(PRQScene scene,
                                     int geomID,
                                     size_t vertexCount,
                                     const void *vertex,
                                     size_t vertexSize,
                                     size_t indexCount,
                                     const void *index,
                                     size_t indexSize);

/*! create a triangle geometry specified through a single array of
  "fat" triangles (ie, each triangle struct directly contains the
  three vertices */
PRQ_API void prqGeometryTriangles(PRQScene scene,
                                  int geomID,
                                  size_t triangleCount,
                                  const void *triangles,
                                  size_t sizeOfTriangle=9*sizeof(float),
                                  size_t sizeOfVertex=3*sizeof(float),
                                  size_t offsetFirstVertex=0);

PRQ_API void prqGeometrySetMask(PRQScene scene,
                                int geomID,
                                PRQMask mask);
PRQ_API void prqGeometryDestroy(PRQScene scene,
                                int geomID);


// ==================================================================
// rays and intersectoins
// ==================================================================

struct PRQRay
{
  prq3f org;
  float tmin;
  prq3f dir;
  float tmax;
};

/*! primID == -1 means 'no hit' */
struct PRQIsec {
  int instID;
  int geomID;
  int primID;
  float t;
  float u;
  float v;
  int reserved[2];
};


// ==================================================================
// "direct" trace api - all data originates on the host, and ends up
// on the host
// ==================================================================

typedef struct _PRQEvent *PRQEvent;

/*! perform a closest hit operation on given rays; after completion of
  this operation the 'isec' array should contain, for each passed
  ray, the closest inteserction in the ray.{tmin,tmax} interval */
PRQ_API PRQEvent prqFindClosestHit(PRQScene  scene,
                                   PRQRay   *rays,
                                   PRQIsec  *isecs,
                                   size_t    count);

PRQ_API PRQEvent prqTestOcclusion(PRQScene  scene,
                                  PRQRay   *rays,
                                  bool     *isecs,
                                  size_t    count);

/*! type that (transparently) keeps all state required for a next-hit
  operation. must be released explicitly by the user */
typedef struct _PRQNextHit *PRQNextHit;

/*! begin a next-hit operation with current set of rays and target
  isec array */
PRQ_API PRQNextHit prqNextHitCreate(PRQRay  *rays,
                                    PRQIsec *isecs,
                                    size_t   count// ,
                                    // PRQMask  mask
                                    );

/*! termiante next-hit operation, and release all interanal state */
PRQ_API void prqNextHitDestroy(PRQNextHit);

/*! execute next-hit trace operation. at the end of this operaiton the
  original 'isec' array passed to prqNextHitCreate will contain the
  respectively next hit along the similarly passed rays */
PRQ_API PRQEvent prqNextHitNext(PRQNextHit);

/*! terminate a next-hit operation, and free all allocated state */
PRQ_API void prqNextHitTerminate(PRQEvent event);

/*! waits for an event to complete, and returns number of rays that
  did have a valid intersection */
PRQ_API size_t prqWaitFor(PRQEvent);






// ==================================================================
// "continuation" trace api - all data originates on the host, and ends up
// on the host
// ==================================================================

