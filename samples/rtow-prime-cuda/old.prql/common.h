// ======================================================================== //
// Copyright 2019 Ingo Wald                                                 //
//                                                                          //
// Licensed under the Apache License, Version 2.0 (the "License");          //
// you may not use this file except in compliance with the License.         //
// You may obtain a copy of the License at                                  //
//                                                                          //
//     http://www.apache.org/licenses/LICENSE-2.0                           //
//                                                                          //
// Unless required by applicable law or agreed to in writing, software      //
// distributed under the License is distributed on an "AS IS" BASIS,        //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. //
// See the License for the specific language governing permissions and      //
// limitations under the License.                                           //
// ======================================================================== //

#pragma once

#include "gdt/math/box.h"
#include "gdt/math/AffineSpace.h"
//#include "gdt/parallel/parallel_for.h"
// std
#include <vector>
#include <iostream>

namespace prql {
  using gdt::vec3f;
  using gdt::vec3i;
  using gdt::affine3f;
  using gdt::xfmPoint;

#define NOTIMPLEMENTED throw std::runtime_error("not implemented");
}
